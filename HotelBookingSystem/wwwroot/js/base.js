function initMap() {
    var room_location = { lat: room_lat, lng: room_lng };
    var map = new google.maps.Map(
        document.getElementById('map'), { zoom: 16, center: room_location });
    var marker = new google.maps.Marker({ position: room_location, map: map });
}

$(function () {

    $('i.star-rating').on('click', function () {
        var count = $(this).attr('name');

        for (var i = 0; i <= 5; i++) {
            if (i < count) 
                $('i.star-rating').eq(i).addClass('star-full-overide fa-star').removeClass('fa-star-o');
            else 
                $('i.star-rating').eq(i).removeClass('star-full-overide fa-star').addClass('fa-star-o');  
        }

        $('#review-rate').val(count);
    });
});

function validateDates() {

    document.getElementById('checkin').classList.remove("invalid-field");
    document.getElementById('checkout').classList.remove("invalid-field");

    document.getElementById('errors').innerHTML = "";
    var checkin = document.getElementById('checkin');
    var checkinValue = checkin.value;
    var checkout = document.getElementById('checkout');
    var checkoutValue = checkout.value;

    if ((checkinValue) && (checkoutValue))
    {
        if (checkinValue <= checkoutValue)
        {
            return true;
        }
        else
        {
            document.getElementById('errors').innerHTML += "<br>" + "<li class='fa'>&#xf071;</li> "+"Invalid Dates<br>";
            document.getElementById('checkin').classList.add("invalid-field");
            document.getElementById('checkout').classList.add("invalid-field");
            return false;
        }
    }
    return true;
}