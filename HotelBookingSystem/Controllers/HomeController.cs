﻿using HotelBookingSystem.Models;
using HotelBookingSystem.ViewModels;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace HotelBookingSystem.Controllers
{
    public class HomeController : Controller
    {
        private readonly WdaContext db;

        public HomeController(WdaContext db)
        {
            this.db = db;
        }

        public IActionResult Index()
        {
            // Get all cities and store in ViewData
            ViewData["Cities"] = db.Room.Select(room => room.City).Distinct();
            // Get all room types and store in ViewData
            ViewData["RoomTypes"] = db.RoomType;

            return View();
        }

        public IActionResult SearchRoom(HomeFiltersViewModel filters)
        {

            if (!ModelState.IsValid)
                throw new ApplicationException("Invalid filters");

            var results = db.Room.Include(room => room.RoomType).AsQueryable();

            if (!string.IsNullOrEmpty(filters.City))
                results = results.Where(room => room.City == filters.City);
            if (filters.RoomTypeId != null)
                results = results.Where(room => room.RoomTypeId == filters.RoomTypeId);
            if (filters.CountOfGuests > 0)
                results = results.Where(room => room.CountOfGuests == filters.CountOfGuests);
            if (filters.MinPrice > 0)
                results = results.Where(room => room.Price >= filters.MinPrice);
            if (filters.MaxPrice > 0)
                results = results.Where(room => room.Price <= filters.MaxPrice);

            if (!string.IsNullOrEmpty(filters.CheckInDate) &&
                !string.IsNullOrEmpty(filters.CheckOutDate) &&
                filters.CheckInDate.CompareTo(filters.CheckOutDate) <= 0)
            {
                // Find all bookings in date range
                // Dates are stored in the database in the format: yyyy-mm-dd
                // So to compare, we compare them as strings
                var bookings = db.Bookings.Where(booking =>
                    booking.CheckInDate.CompareTo(filters.CheckOutDate) <= 0 &&
                    filters.CheckInDate.CompareTo(booking.CheckOutDate) <= 0);

                // Return all rooms except the ones contained in the bookings collection
                results = results.Where(i => !bookings.Any(b => b.RoomId == i.RoomId));
            }
            SearchRoomViewModel srvModel = new SearchRoomViewModel();

            ViewData["CntOfGuests"] = db.Room.Select(room => room.CountOfGuests).Distinct();
            ViewData["Cities"] = db.Room.Select(room => room.City).Distinct();
            ViewData["RoomTypes"] = db.RoomType;

            srvModel.Rooms = results;
            srvModel.Filters = filters;
            return View(srvModel);
        }

        public IActionResult Room(int id,BookingFiltersViewModel bookingFilters)
        {
            var userid = this.User.Identity.IsAuthenticated ? int.Parse(this.User.FindFirst(ClaimTypes.NameIdentifier).Value) : -1;

            var room = db.Room
                .Include(r => r.RoomType)
                .Include(r => r.Reviews)
                    .ThenInclude(u => u.User)
                .Where(r => r.RoomId == id).SingleOrDefault();

            var favorite = db.Favorites.Where(r => r.RoomId == id && r.UserId == userid).SingleOrDefault();

            if (room == null)
                return RedirectToAction("Index", "Home");

            var model = new RoomViewModel
            {
                Room = room,
                IsFavorite = favorite == null ? false:true,
                BookingFilters = bookingFilters,
                ReviewFilters = new ReviewFiltersViewModel {
                    Rate = 1,//minimum rating of 1 star by default
                    RoomId = id,
                },
                FavoritesFilters = new FavoritesFiltersViewModel{
                    RoomId = id,
                },
                ShowBookingButton = !string.IsNullOrEmpty(bookingFilters.CheckInDate) &&
                                    !string.IsNullOrEmpty(bookingFilters.CheckOutDate)
            };
            model.BookingFilters.RoomId = id;

            // Find all bookings in date range
            // Dates are stored in the database in the format: yyyy-mm-dd
            // So to compare, we compare them as strings
            var bookings = db.Bookings.Where(booking =>
                booking.CheckInDate.CompareTo(bookingFilters.CheckOutDate) <= 0 &&
                bookingFilters.CheckInDate.CompareTo(booking.CheckOutDate) <= 0 &&
                booking.RoomId == id);

            model.IsAvailable = !bookings.Any();

            return View(model);
        }

        [HttpPost]
        public IActionResult SubmitBooking(BookingFiltersViewModel bookingFilters)
        {
            var userid = this.User.Identity.IsAuthenticated ? int.Parse(this.User.FindFirst(ClaimTypes.NameIdentifier).Value) : -1;

            db.Bookings.Add(new Bookings
            {
                CheckInDate = bookingFilters.CheckInDate,
                CheckOutDate = bookingFilters.CheckOutDate,
                DateCreated = DateTime.Now,
                RoomId = bookingFilters.RoomId,
                UserId = userid,
            });
            db.SaveChanges();

            return RedirectToAction("Room", "Home", new { id = bookingFilters.RoomId });
        }

        [HttpPost]
        public IActionResult SubmitReview(ReviewFiltersViewModel ReviewFilters)
        {
            var userid = this.User.Identity.IsAuthenticated ? int.Parse(this.User.FindFirst(ClaimTypes.NameIdentifier).Value) : -1;

            db.Reviews.Add(new Reviews
            {
                Rate = ReviewFilters.Rate,
                Text = ReviewFilters.Text,
                DateCreated = DateTime.Now,
                RoomId = ReviewFilters.RoomId,
                UserId = userid,
            });
            db.SaveChanges();

            return RedirectToAction("Room", "Home",new {id= ReviewFilters.RoomId });
        }

        [HttpPost]
        public IActionResult ToggleFavorite(FavoritesFiltersViewModel FavoritesFilters)
        {
            var userid = this.User.Identity.IsAuthenticated ? int.Parse(this.User.FindFirst(ClaimTypes.NameIdentifier).Value) : -1;

            try
            {
                var favorite = db.Favorites.FirstOrDefault(fav => fav.RoomId == FavoritesFilters.RoomId && fav.UserId == userid);

                if (favorite == null)
                {
                    db.Favorites.Add(new Favorites
                    {
                        RoomId = FavoritesFilters.RoomId,
                        UserId = userid,
                        Status = 1,
                        DateCreated = DateTime.Now,
                    });
                }
                else
                {
                    db.Favorites.Remove(favorite);
                }
                db.SaveChanges();

                return RedirectToAction("Room", "Home", new { id = FavoritesFilters.RoomId });
                //return Json(true);
            }
            catch
            {
                return RedirectToAction("Room", "Home", new { id = FavoritesFilters.RoomId });
                //return Json(false);
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
