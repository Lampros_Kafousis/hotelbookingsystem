﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HotelBookingSystem.Models;
using HotelBookingSystem.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;

namespace HotelBookingSystem.Controllers
{
    public class MyProfileController : Controller
    {
        private readonly WdaContext db;
        private readonly SignInManager<User> _signInManager;

        public MyProfileController(WdaContext db, SignInManager<User> signInManager)
        {
            this.db = db;
            this._signInManager = signInManager;
        }

        
        [HttpGet]
        public IActionResult Login() {
            return View();
        }

        [HttpPost]
        public IActionResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid) {

                var result = _signInManager.PasswordSignInAsync(model.Username, model.Password, model.RememberMe, false).Result;

                if (result.Succeeded) {
                    return RedirectToAction("Index", "MyProfile");
                } else {
                    return RedirectToAction("Index", "Home");
                }

            }

            ModelState.AddModelError("", "Invalid login attempt");

            return View(model);
        }

        [HttpGet]
        public IActionResult Logout() {

            if (ModelState.IsValid) {
                var result = _signInManager.SignOutAsync();
            }

            return RedirectToAction("Index", "Home");
        }


        [Authorize]
        public IActionResult Index()
        {
            var userid = this.User.Identity.IsAuthenticated ? int.Parse(this.User.FindFirst(ClaimTypes.NameIdentifier).Value) : -1;

            var user = db.User
                    .Include(u => u.Reviews)
                        .ThenInclude(r => r.Room)
                    .Include(u => u.Bookings)
                        .ThenInclude(r => r.Room)
                            .ThenInclude(rt => rt.RoomType)
                    .Include(u => u.Favorites)
                        .ThenInclude(r => r.Room)
                    .Where(u => u.UserId == userid).SingleOrDefault();


            return View(user);
    }
}
}