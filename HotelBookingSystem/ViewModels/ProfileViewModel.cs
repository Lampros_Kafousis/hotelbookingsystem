﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelBookingSystem.Models;

namespace HotelBookingSystem.ViewModels
{
    public class ProfileViewModel
    {
        public User User { get; set; }
    }
}
