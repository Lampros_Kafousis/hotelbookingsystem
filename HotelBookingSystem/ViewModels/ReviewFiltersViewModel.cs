﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace HotelBookingSystem.ViewModels
{
    public class ReviewFiltersViewModel
    {
        [Display(Name = "Rate")]
        public int Rate { get; set; }

        [Display(Name = "RoomId")]
        public int RoomId { get; set; }

        [Display(Name = "Text")]
        public string Text { get; set; }
    }
}
