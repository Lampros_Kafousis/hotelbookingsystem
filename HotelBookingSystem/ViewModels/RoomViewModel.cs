﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelBookingSystem.Models;

namespace HotelBookingSystem.ViewModels
{
    public class RoomViewModel
    {
        public Room Room { get; set; }
        public ReviewFiltersViewModel ReviewFilters { get; set; }
        public BookingFiltersViewModel BookingFilters { get; set; }
        public FavoritesFiltersViewModel FavoritesFilters { get; set; }

        public bool ShowBookingButton { get; set; }
        public bool IsAvailable { get; set; }
        public bool IsFavorite { get; set; }
    }
}
