﻿using System.ComponentModel.DataAnnotations;

namespace HotelBookingSystem.ViewModels
{
    public class HomeFiltersViewModel
    {
        [Required]
        [Display(Name = "City")]
        public string City { get; set; }

        [Required]
        [Display(Name = "Room Type")]
        public int? RoomTypeId { get; set; }

        [Required]
        [Display(Name = "Check In Date")]
        public string CheckInDate { get; set; }

        [Required]
        [Display(Name = "Check Out Date")]
        public string CheckOutDate { get; set; }

        [Display(Name = "Count Of Guests")]
        public int CountOfGuests { get; set; }

        [Display(Name = "Minimum Price")]
        public int MinPrice { get; set; }

        [Display(Name = "Maximum Price")]
        public int MaxPrice { get; set; }
    }
}
