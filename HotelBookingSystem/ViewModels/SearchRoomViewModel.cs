﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HotelBookingSystem.ViewModels
{
    public class SearchRoomViewModel
    {
        public IEnumerable<Models.Room> Rooms { get; set; }
        public HomeFiltersViewModel Filters { get; set; }
    }
}
