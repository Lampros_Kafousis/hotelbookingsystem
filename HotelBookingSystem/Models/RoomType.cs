﻿using System;
using System.Collections.Generic;

namespace HotelBookingSystem.Models
{
    public partial class RoomType
    {
        public int Id { get; set; }
        public string RoomType1 { get; set; }
        public IEnumerable<Room> Rooms { get; set; }
    }
}
