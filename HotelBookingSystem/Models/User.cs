﻿using System;
using System.Collections.Generic;

namespace HotelBookingSystem.Models
{
    public partial class User
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }

        public IEnumerable<Reviews> Reviews { get; set; }
        public IEnumerable<Bookings> Bookings { get; set; }
        public IEnumerable<Favorites> Favorites { get; set; }
    }
}
